function modifyProjectState(pid) {
	var element = $("#modifyprojectstate" + pid);
	var content = "<ul class='inline'><li><a href='#' onclick='validateProject(" + pid + ")'>Valider</a></li><li><a href='#' onclick='invalidateProject(" + pid + ")'>Invalider</a><li><a href='#' onclick='refuseProject(" + pid + ")'>Refuser</a></li></ul>"
	element.html(content);
}

function validateProject(pid) {
	var element = $("#modifyprojectstate" + pid);
	$.ajax({
		url: "/admin/projects/validate/" + pid,
		error: function(xhr) {
			alert("/admin/projects/validate/" + pid);
		}
	}).done(function ( data ) {
		if(data == "ok") {
			element.html("<a href='#' onclick='modifyProjectState("+pid+")'><span class='label label-warning'>En attente de résurrection</span><i class='icon-edit'></i></a>");
		} else {
			element.html(element.html + data);
		}
	});
}

function invalidateProject(pid) {
	var element = $("#modifyprojectstate" + pid);
	$.ajax({
		url: "/admin/projects/invalidate/" + pid,
		error: function(xhr) {
			alert("/admin/projects/validate/" + pid);
		}
	}).done(function ( data ) {
		if(data == "ok") {
			element.html("<a href='#' onclick='modifyProjectState("+pid+")'><span class='label'>En attente de validation</span><i class='icon-edit'></i></a>");
		} else {
			element.html(element.html + data);
		}
	});
}

function refuseProject(pid) {
	var element = $("#modifyprojectstate" + pid);
	$.ajax({
		url: "/admin/projects/refuse/" + pid,
		error: function(xhr) {
			alert("/admin/projects/validate/" + pid);
		}
	}).done(function ( data ) {
		if(data == "ok") {
			element.html("<a href='#' onclick='modifyProjectState(@project.id)'><span class='label label-important'>Refusé</span><i class='icon-edit'></i></a>");
		} else {
			element.html(element.html + data);
		}
	});
}