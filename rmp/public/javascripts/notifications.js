function seeRead() {
	$("#readButton").addClass("active");
	$("#unreadButton").removeClass("active");
	$("#readNotif").show();
	$("#unreadNotif").hide();
}

function seeUnread() {
	$("#readButton").removeClass("active");
	$("#unreadButton").addClass("active");
	$("#readNotif").hide();
	$("#unreadNotif").show();
}

function markNotifAsRead(id) {
	var element = $("#notification"+id);
	$.ajax({
		url: "/myaccount/notifications/markasread/" + id,
		error: function(xhr) {
			alert(xhr);
		}
	}).done(function ( data ) {
		if(data == "ok") {
			element.hide("slow", element.remove)
			$("#readNotif").prepend(element.html());
			element.find(".markasreadlink").remove();
			var nbNotifs = parseInt($(".notifcounter").html());
			$(".notifcounter").html(nbNotifs - 1);
		} else {
		}
	});
}

function removeNotif(id) {
	var element = $("#notification"+id);
	$.ajax({
		url: "/myaccount/notifications/remove/" + id,
		error: function(xhr) {
			alert(xhr);
		}
	}).done(function ( data ) {
		if(data == "ok") {
			element.hide("slow", element.remove)
		} else {
		}
	});
}