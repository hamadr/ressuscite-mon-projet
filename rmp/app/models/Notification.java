package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.data.format.Formats;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Notification extends Model {
	@Id
	@GeneratedValue
	public int id;
	
	@ManyToOne
	public User recipient;
	
	public String subject;
	@Column(columnDefinition = "varchar(1000)")
	public String message;
	public boolean read;
	@Formats.DateTime(pattern="dd/MM/yyyy")
	public Date date;
	
	public static Finder<String, Notification> find = new Finder<String,Notification>(
			String.class, Notification.class
			);
	
	public Notification(User recipient, String subject, String message) {
		this.recipient = recipient;
		this.subject = subject;
		this.message = message;
		this.read = false;
		this.date = new Date();
	}
}
