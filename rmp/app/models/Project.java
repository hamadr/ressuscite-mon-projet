package models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.avaje.ebean.annotation.Formula;

import play.data.format.Formats;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name="PROJECTS")
public class Project extends Model {
	public enum enumStates {AWAITINGVALIDATION, REFUSED, AWAITINGRES, RES}

	@Id
	@GeneratedValue
	public int id;
	@Required
	public String name;
	@Required
	public String description;
	public String website;
	public enumStates currentState;
	@Formats.DateTime(pattern="dd/MM/yyyy")
	public Date creationDate;
	@ManyToOne
	public Category category;
	
	@ManyToOne
	public User creator;// never null
	@ManyToMany(mappedBy="projectsFollowed")
	@JoinTable(name="user_proj_encouragement")
	public Set<User> followers = new HashSet<User>();
	@ManyToMany(mappedBy="projectsContributed")
	@JoinTable(name="user_proj_contribution")
	public Set<User> contributors = new HashSet<User>();
	@OneToMany(mappedBy="project", cascade = CascadeType.ALL)
	public Set<Reward> rewards = new HashSet<Reward>();
	@OneToMany(mappedBy="project", cascade = CascadeType.ALL)
	public Set<RezQuery> propResSet = new HashSet<RezQuery>();;
	@ManyToOne
	public User resurrector;// may be null
	@Transient
	@Formula(select="t1.nbF", join="join(SELECT projects_id, count(*) AS nbF from USER_PROJ_ENCOURAGEMENT group by projects_id) as t1 on t1.projects_id=t0.id")
	public int nbFollowers;
	@Transient
	@Formula(select="t1.tamount", join="join(SELECT project_id, sum(amount) AS tamount from REWARDS group by project_id) as t1 on t1.project_id=t0.id")
	public int rewardsTotalAmount;
	public static Finder<String, Project> find = new Finder<String,Project>(
			String.class, Project.class
			);
}
