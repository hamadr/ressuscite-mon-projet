package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name="CATEGORIES")
public class Category extends Model {

	@Id
	@GeneratedValue
	public int id;
	@Column(unique = true)
	public String name;
	@Required
	public String description;
	
	@OneToMany(mappedBy="category", cascade = CascadeType.ALL)
	public Set<Project> projects = new HashSet<Project>();
	
	public static Finder<String, Category> find = new Finder<String,Category>(
			String.class, Category.class
			);
	
	public String getImageName() {
		return name.replace('é', 'e');
	}
}
