package models;

import javax.persistence.*;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name="REWARDS")
public class Reward extends Model {
	
	@Id
	@GeneratedValue
	public int id;
	
	public int amount;
	@ManyToOne
	@Column(unique = true)
	public User rewarder;
	@ManyToOne
	@Column(unique = true)
	public Project project;
	
	public static Finder<String, Reward> find = new Finder<String,Reward>(
			String.class, Reward.class
			);
	
	public Reward(User user, Project project, int amount) {
		this.rewarder = user;
		this.project = project;
		this.amount = amount;
	}
	
	public Reward(){
		
	}
}
