package models;

import javax.persistence.*;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(name="REZQUERY")
public class RezQuery extends Model {
	@Id
	@GeneratedValue
	public int id;
	@ManyToOne
	public User user;
	@ManyToOne
	public Project project;
	public String motivation;
	
	public static Finder<String, RezQuery> find = new Finder<String,RezQuery>(
			String.class, RezQuery.class
			);
	
	public RezQuery(User user, Project project, String motivation) {
		super();
		this.user = user;
		this.project = project;
		this.motivation = motivation;
	}
	public RezQuery() {
		
	}
}
