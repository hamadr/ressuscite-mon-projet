package models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.Crypto;
import java.io.*;
import java.security.*;
import play.data.format.*;
import play.db.ebean.Model.Finder;

class MD5Util {   
	public static String hex(byte[] array) {       
		StringBuffer sb = new StringBuffer();       
		for (int i = 0; i < array.length; ++i) {           
			sb.append(Integer.toHexString((array[i]
					& 0xFF) | 0x100).substring(1,3));       
		}       
		return sb.toString();   
	}   
	public static String md5Hex (String message) {        
		try {            
			MessageDigest md =
					MessageDigest.getInstance("MD5");            
			return hex (md.digest(message.getBytes("CP1252")));        
		} catch (NoSuchAlgorithmException e) {        
		} catch (UnsupportedEncodingException e) {        
		}        
		return null;   
	}   
}

@Entity
@Table(name="USERS")
public class User extends Model {
	@Id
	@Required
	public String email;
	@Required
	@Column(unique = true)
	public String pseudo;
	@Required
	public String password;
	public String webPage;
	public boolean isAdmin;
	@Formats.DateTime(pattern="dd/MM/yyyy")
	public Date dateInscription = new Date();

	@OneToMany(mappedBy="creator", cascade = CascadeType.ALL)
	@JoinTable(name="user_proj_creation")
	public Set<Project> projectsCreated = new HashSet<Project>();
	@ManyToMany
	@JoinTable(name="user_proj_encouragement")
	public Set<Project> projectsFollowed = new HashSet<Project>();
	@ManyToMany
	@JoinTable(name="user_proj_contribution")
	public Set<Project> projectsContributed = new HashSet<Project>();
	@OneToMany(mappedBy="resurrector", cascade = CascadeType.ALL)
	@JoinTable(name="user_proj_resurrection")
	public Set<Project> projectsResurrected = new HashSet<Project>();
	@OneToMany(mappedBy="rewarder", cascade = CascadeType.ALL)
	public Set<Reward> rewards = new HashSet<Reward>();
	@OneToMany(mappedBy="user", cascade = CascadeType.ALL)
	public Set<RezQuery> rezQuerySet = new HashSet<RezQuery>();
	@OneToMany(mappedBy="recipient", cascade = CascadeType.ALL)
	public Set<Notification> notifications = new HashSet<Notification>();

	public void hashPassword() {
		this.password = hashPassword(this.password);
	}
	public static String hashPassword(String passwd) {
		return Crypto.sign(passwd);
	}

	//to use gravatar
	public String getAvatarLink() {
		return getAvatarLink(email);
	}

	//to use gravatar
	public static String getAvatarLink(String email) {
		return new String("http://www.gravatar.com/avatar/" + MD5Util.md5Hex(email).toLowerCase());
	}
	@Override
	public boolean equals(Object arg0) {
		if(arg0 instanceof User) {
			return ((User)arg0).email == this.email;
		}
		return false;
	}
	public static Finder<String,User> find = new Finder<String,User>(
			String.class, User.class
			);
}
