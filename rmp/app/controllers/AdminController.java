package controllers;

import models.Notification;
import models.Project;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;

public class AdminController extends Controller {
	static boolean isAdmin() {
		User user;
		try {
			user = User.find.byId(session("usermail"));
		} catch(NullPointerException e) {
			return false;
		}
		if(user == null)
			return false;
		if(!user.isAdmin)
			return false;
		return true;
	}
	
	public static Result validateProject(String pid) {
		if(!isAdmin())
			return badRequest();
		Project p = Project.find.byId(pid);
		if(p == null)
			return ok("existe pas");
		p.currentState = Project.enumStates.AWAITINGRES;
		p.save();
		User user = p.creator;
		user.notifications.add(new Notification(user, "Projet validé", "Votre projet " + p.name + " est maintenant validé !"));
		user.save();
		return ok("ok");
	}
	public static Result invalidateProject(String pid) {
		if(!isAdmin())
			return badRequest();
		Project p = Project.find.byId(pid);
		if(p == null)
			return ok("existe pas");
		p.currentState = Project.enumStates.AWAITINGVALIDATION;
		p.save();
		User user = p.creator;
		user.notifications.add(new Notification(user, "Projet en attente de validation", "Votre projet " + p.name + " a été repassé en attente de validation. Contactez un administrateur pour en savoir plus."));
		user.save();
		return ok("ok");
	}
	public static Result refuseProject(String pid) {
		if(!isAdmin())
			return badRequest();
		Project p = Project.find.byId(pid);
		if(p == null)
			return ok("existe pas");
		p.currentState = Project.enumStates.REFUSED;
		p.save();
		User user = p.creator;
		user.notifications.add(new Notification(user, "Projet refusé", "Votre projet " + p.name + " a été refusé. Contactez un administrateur pour en savoir plus."));
		user.save();
		return ok("ok");
	}
}
