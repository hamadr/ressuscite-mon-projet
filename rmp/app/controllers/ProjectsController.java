package controllers;

import java.util.Date;

import models.Category;
import models.Notification;
import models.Project;
import models.Reward;
import models.RezQuery;
import models.User;
import models.Project.enumStates;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
public class ProjectsController extends Controller {
	
	public static Result postProject() {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		
		Form<Project> form = Form.form(Project.class).bindFromRequest();
		if(form.hasErrors()) {
			flash("popupMsg", form.errorsAsJson().toString());
			return redirect(routes.Application.postProject());
		}
		Project project = form.get();
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		project.currentState = enumStates.AWAITINGVALIDATION;
		project.creator = user;
		project.category = new Model.Finder<String, Category>(String.class, Category.class).byId(requestData.get("category-id"));
		project.creationDate = new Date();
		project.save();
		user.projectsCreated.add(project);
		user.notifications.add(new Notification(user, "Projet en attente de validation", "Votre projet " + project.name + " est maintenant en attente de validation."));
		user.save();
		return redirect(routes.Application.project(Integer.toString(project.id)));
	}
	
	public static Result follow(String id) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		Project project;
		try {
			project = Project.find.byId(id);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));

		project.followers.add(user);
		project.save();
		return redirect(routes.Application.project(id));
	}
	
	public static Result unfollow(String id) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		
		Project project;
		try {
			project = Project.find.byId(id);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		project.followers.remove(user);
		project.save();
		return redirect(routes.Application.project(id));
	}
	
	public static Result addReward(String id) {
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		String amount = requestData.get("amount");
		if(amount.isEmpty())
			return badRequest();
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		
		Project project;
		try {
			project = Project.find.byId(id);
		} catch(NullPointerException e) {
			return redirect(routes.Application.project(id));
		}
		if(project == null)
			return redirect(routes.Application.project(id));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return redirect(routes.Application.project(id));
		Reward r = new Reward(user, project, Integer.parseInt(amount));
		r.save();
		project.rewards.add(r);
		project.save();
		return redirect(routes.Application.project(id));
	}
	
	public static Result removeReward(String idreward) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		
		Reward reward = Reward.find.byId(idreward);
		if(reward.rewarder.email != user.email)
			return ok("nope");
		reward.delete();
		return ok("ok");
	}

	public static Result contribute(String idProject) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		Project project;
		try {
			project = Project.find.byId(idProject);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));

		project.contributors.add(user);
		project.save();
		return redirect(routes.Application.project(idProject));
	}
	
	public static Result uncontribute(String idProject) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		Project project;
		try {
			project = Project.find.byId(idProject);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));

		project.contributors.remove(user);
		project.save();
		return redirect(routes.Application.project(idProject));
	}
	
	public static Result addRez(String idProject) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		Project project;
		try {
			project = Project.find.byId(idProject);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		String motiv = requestData.get("motivations");
		RezQuery rq = new RezQuery(user, project, motiv);
		rq.save();
		project.propResSet.add(rq);
		project.save();
		Notification nCreateur = new Notification(project.creator, "Demande de reprise", user.pseudo + " est intéressé pour reprendre votre projet " + project.name + ". Allez lire sa candidature !");
		Notification nDemandeur = new Notification(user, "Demande de reprise", "Votre demande de reprise a été prise en compte et doit être validée par " + project.creator.pseudo + ".");
		nCreateur.save();
		nDemandeur.save();
		return redirect(routes.Application.project(idProject));
	}
	
	public static Result modifyRez(String idRez) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		RezQuery rq;
		try {
			rq = RezQuery.find.byId(idRez);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(rq == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(rq.user.email != user.email)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		Project project = rq.project;
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));

		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		rq.motivation = requestData.get("motivations");
		rq.update();
		return redirect(routes.Application.project(Integer.toString(rq.project.id)));
	}
	
	public static Result deleteRez(String idRez) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		RezQuery rq;
		try {
			rq = RezQuery.find.byId(idRez);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(rq == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(rq.user.email != user.email)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		Project project = rq.project;
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		rq.delete();
		return redirect(routes.Application.project(Integer.toString(rq.project.id)));
	}
	
	
	public static Result setRez(String idRez) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		RezQuery rq;
		try {
			rq = RezQuery.find.byId(idRez);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(rq == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		Project project = rq.project;
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.creator.email != user.email) 
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		project.resurrector = rq.user;
		project.currentState = Project.enumStates.RES;
		project.save();
		Notification nDemandeur = new Notification(rq.user, "Demande de reprise acceptée", "Votre demande de reprise pour le projet " + rq.project.name + " a été acceptée !");
		nDemandeur.save();
		rq.delete();
		return redirect(routes.Application.project(Integer.toString(rq.project.id)));
	}
	
	public static Result cancelRez(String idProject) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		Project project;
		try {
			project = Project.find.byId(idProject);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.RES)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.resurrector.email != user.email) 
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		project.resurrector = null;
		project.currentState = Project.enumStates.AWAITINGRES;
		Notification nCreateur = new Notification(project.creator, "Annulation de résurrection", user.pseudo + " a annulé sa résurrection de " + project.name + ". Vous pouvez néanmoins lire les anciennes candidatures et voir les nouvelles.");
		nCreateur.save();
		project.save();
		return redirect(routes.Application.project(idProject));
	}
}
