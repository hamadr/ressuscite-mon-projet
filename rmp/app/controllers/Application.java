package controllers;

import play.mvc.*;
import views.html.*;

import java.util.*;

import models.Category;
import models.Project;
import models.RezQuery;
import models.User;

public class Application extends Controller {
	static User getCurrentUser() {
		User user;
		try {
			user = User.find.byId(session("usermail"));
		} catch (NullPointerException e) {
			user = null;
		}
		return user;
	}
	
    public static Result index() {
   		List<Project> projects = 
   				Project.find
   				.select("id, name, description, category, nbFollowers")
   				.where()
   				.eq("currentState", Project.enumStates.AWAITINGRES)
   				.orderBy("nbFollowers desc")
   				.setMaxRows(3).
   				findList();
        return ok(index.render(projects));
    }
    
    public static Result login() {
    	return ok(login.render());
    }
    
    public static Result subscribe() {
    	return ok(subscribe.render());
    }
    public static Result about() {
    	return ok(about.render());
    }
    
	public static Result categoryHome() {
		Project mostFollowed = 
   				Project.find
   				.select("id, name, description, category, nbFollowers")
   				.where()
   				.eq("currentState", Project.enumStates.AWAITINGRES)
   				.orderBy("nbFollowers desc")
   				.setMaxRows(1).findUnique();
		Project mostRewarded = 
   				Project.find
   				.select("id, name, description, category, rewardsTotalAmount")
   				.where()
   				.eq("currentState", Project.enumStates.AWAITINGRES)
   				.orderBy("rewardsTotalAmount desc")
   				.setMaxRows(1).findUnique();
   				
		Project mostRecent = 
   				Project.find
   				.select("id, name, description, category, creationDate")
   				.where()
   				.eq("currentState", Project.enumStates.AWAITINGRES)
   				.orderBy("creationDate desc")
   				.setMaxRows(1).findUnique();
   				
		return ok(categoryhome.render(mostFollowed, mostRewarded, mostRecent));
	}
	public static Result allProjects() {
		return ok(allprojects.render());
	}
	public static Result category(String id, String sort) {
		return ok(categorybrowser.render(Category.find.byId(id)));
	}
	
	public static Result project(String id) {
		Project project;
		try {
			project = Project.find.byId(id);
		} catch(NullPointerException e) {
			return ok(error.render("Projet introuvable", "Le projet que vous cherchez est introuvable ! :("));
		}
		if(project == null)
			return ok(error.render("Projet introuvable", "Le projet que vous cherchez est introuvable ! :("));
		
		User user;
		try {
			user = User.find.byId(session("usermail"));
		} catch(NullPointerException e) {
			user = new User();
		}
		if(!user.isAdmin && project.currentState != Project.enumStates.AWAITINGRES && project.currentState != Project.enumStates.RES)
			if(project.creator.email != user.email)
				return ok(error.render("Projet non validé", "Ce projet n'a pas été validé !"));
		return ok(viewproject.render(project, user));
	}
	
	public static Result rezquery(String id) {
		RezQuery rezquery;
		try {
			rezquery = RezQuery.find.byId(id);
		} catch(NullPointerException e) {
			return ok(error.render("Proposition de reprise introuvable", "Le proposition de reprise que vous cherchez est introuvable ! :("));
		}
		if(rezquery == null)
			return ok(error.render("Proposition de reprise introuvable", "Le proposition de reprise est introuvable ! :("));
		
		User user = Application.getCurrentUser();
		if(user.email != rezquery.user.email)
			return ok(error.render("Vous n'avez pas le droit de consulter cette proposition de reprise", "Vous n'avez pas le droit de consulter cette proposition de reprise ! :("));

		return ok(viewrezquery.render(rezquery, user));
	}
	
	public static Result myaccount(String category) {
		if(!session().containsKey("usermail")) {
			flash("popupMsg", "Vous devez être connecté !");
			return redirect(routes.Application.login());
		}
		User user;
		try {
			user = User.find.byId(session("usermail"));
		} catch(NullPointerException e) {
			return badRequest();
		}
		if(user == null)
			return ok(error.render("Nope !", "Nope !"));
		if(category.equals("info"))
			return ok(myaccount_info.render(user));
		else if(category.equals("notifications"))
			return ok(myaccount_notifications.render(user));
		else if(category.equals("myprojects"))
			return ok(myaccount_projects.render(user));
		else if(category.equals("myfollowedprojects"))
			return ok(myaccount_followedprojects.render(user));
		else if(category.equals("myqueries"))
			return ok(myaccount_rezqueries.render(user));
		else if(category.equals("contributions"))
			return ok(myaccount_contributions.render(user));
		else if(category.equals("resurrected"))
			return ok(myaccount_rezprojects.render(user));
		else
			return ok(myaccount_info.render(user));
	}
	
	public static Result postProject() {
		if(session().containsKey("usermail")) {
			return ok(postproject.render());
		}
		flash("popupMsg", "Vous devez d'abord vous connecter");
		return ok(login.render());
	}
	
	public static Result viewUser(String pseudo) {
		User user = User.find.where().eq("pseudo", pseudo).findUnique();
		return ok(viewuser.render(user));
	}
	
	public static Result adminUsers() {
		if(!AdminController.isAdmin())
			return ok(error.render("Droits insuffisants", "Vous n'êtes pas administrateur !"));
		return ok(admin_users.render());
	}
	
	public static Result adminProjects() {
		if(!AdminController.isAdmin())
			return ok(error.render("Droits insuffisants", "Vous n'êtes pas administrateur !"));
		return ok(admin_projects.render());
	}
	
	public static Result admin() {
		if(!AdminController.isAdmin())
			return ok(error.render("Droits insuffisants", "Vous n'êtes pas administrateur !"));
		return ok(admin_projects.render());
	}
	
	public static Result manageRez(String idProject) {
		User user = Application.getCurrentUser();
		if(user == null) {
			flash("popupMsg", "Vous devez d'abord vous connecter");
			return ok(login.render());
		}
		Project project;
		try {
			project = Project.find.byId(idProject);
		} catch(NullPointerException e) {
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		}
		if(project == null)
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		if(project.currentState != Project.enumStates.AWAITINGRES)
			return ok(error.render("Oust !", "Le projet est sûrement déjà ressuscité !"));
		if(!project.creator.email.equals(user.email))
			return ok(error.render("Oust !", "Tu ne devrais pas être ici"));
		return ok(managerez.render(project));
	}
}
