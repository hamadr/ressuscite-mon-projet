package controllers;

import java.util.Date;

import models.Notification;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;
import play.libs.Crypto;

public class UserController extends Controller {

	public static Result subscribe() {
		Form<User> userForm = Form.form(User.class).bindFromRequest();
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		if(userForm.hasErrors()) {
			flash("popupMsg", userForm.errorsAsJson().toString());
			return redirect(routes.Application.subscribe());
		}
		if(!requestData.get("password").equals(requestData.get("password2"))) {
			flash("popupMsg", "Les mots de passe doivent être les mêmes !");
			return redirect(routes.Application.subscribe());
		}
		User user = userForm.get();
		user.hashPassword();
		user.dateInscription = (new Date());
		user.isAdmin =(false);
		user.save();
		flash("popupMsg", "Vous pouvez maintenant vous connecter !");
		return redirect(routes.Application.login());
	}
	
	public static Result authenticate() {
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		User user = User.find.byId(requestData.get("email"));
		if(user != null) {
			if(user.password.equals(Crypto.sign(requestData.get("password")))) {
				session().put("usermail", user.email);
				flash("popupMsg", "Vous êtes maintenant connecté");
				return redirect(routes.Application.index());
			}
		}
		flash("popupMsg", "La combinaison renseignée est invalide");
		return redirect(requestData.get("redirect"));
	}
	public static Result logout() {
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		session().clear();
		flash("popupMsg", "Vous êtes maintenant déconnecté");
		return redirect(requestData.get("redirect"));
	}
	
	
	public static Result modify() {
		DynamicForm requestData = DynamicForm.form().bindFromRequest();
		User user = Application.getCurrentUser();
		if(!user.password.equals(User.hashPassword(requestData.get("validatepassword")))) {
			flash("popupMsg", "Le mot de passe actuel renseigné n'a pu être validé");
			return redirect(routes.Application.myaccount("info"));
		}
		String msgRetour = "";
		if(!requestData.get("password").isEmpty()) {
			user.password  = requestData.get("password");
			user.hashPassword();
			msgRetour += "Mot de passe changé<br/>";
		}
		user.webPage = requestData.get("website");
		msgRetour += "Page web changée";
		user.save();
		flash("popupMsg", msgRetour);
		return redirect(routes.Application.myaccount("info"));
	}
	
	public static Result markNotifAsRead(String id) {
		Notification n = Notification.find.byId(id);
		if(Application.getCurrentUser().email != n.recipient.email) {
			return badRequest();
		}
		n.read = true;
		n.save();
		return ok("ok");
	}
	
	public static Result removeNotif(String id) {
		Notification n = Notification.find.byId(id);
		if(Application.getCurrentUser().email != n.recipient.email) {
			return badRequest();
		}
		n.delete();
		return ok("ok");
	}
}
