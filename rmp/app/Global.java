
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import models.Category;
import models.Notification;
import models.Project;
import models.Reward;
import models.Project.enumStates;
import models.RezQuery;
import models.User;
import play.Application;
import play.GlobalSettings;


public class Global extends GlobalSettings {
	private int nbCateg = 7;
	List<Category> categories;
	
	private int nbNotif = 20;
	List<Notification> notifications;
	
	private int nbProj = 20;
	List<Project> projects;
			
	private int nbUsers = 30;
	List<User> users;
	
	Random rand = new Random();
	
	private Date randomDate() {
		int numJour = rand.nextInt(28)+1;
		int numMois = rand.nextInt(12)+1;
		return new Date(numJour,numMois,2012);
	}
	
	private Set<User> randomUserSet(int nbUser) {
		Set<User> userSet = new HashSet<User>(nbUser);
		List<User> userList = new LinkedList<User>(users);
		for (int i = 0; i < nbUser; i++) {
			int iUser = rand.nextInt(userList.size());
			userSet.add(userList.remove(iUser));
		}
		return userSet;
	}

	private void clearDatabase() {
		for (Category categ : Category.find.all())
			categ.delete();
		for (Notification notif : Notification.find.all())
			notif.delete();
		for (Project project : Project.find.all())
			project.delete();
		for (RezQuery propResu : RezQuery.find.all())
			propResu.delete();
		for (Reward reward : Reward.find.all())
			reward.delete();
		for (User user : User.find.all())
			user.delete();
	}
	
	private void createUsers() {
		users = new ArrayList<User>(nbUsers);
		for (int i = 0; i < nbUsers; i++) {
			User user = new User();
			users.add(user);
			
			user.pseudo = "user"+i;
			user.email = user.pseudo+"@gmail.com";
			user.password = User.hashPassword("1234");
			user.webPage = "www."+user.pseudo+".fr";
			user.isAdmin = (i == 0 || i == 1);
			user.dateInscription = randomDate();
			
			user.save();
		}
	}
	
	private void createNotifications() {
		notifications = new ArrayList<Notification>(nbNotif);
	}
	
	private void createCategories() {
		categories = new ArrayList<Category>(nbCateg);
		for (int i = 0; i < nbCateg; i++) {
			categories.add(new Category());
		}
		
		categories.get(0).name = "Multimédia";		
		categories.get(1).name = "Scientifique";
		categories.get(2).name = "Bureautique";
		categories.get(3).name = "Jeux";
		categories.get(4).name = "Gestion";
		categories.get(5).name = "Développement";
		categories.get(6).name = "Internet";

		for (int i = 0; i < nbCateg; i++) {
			Category cat = categories.get(i);
			cat.description = "La catégorie des projets "+cat.name.toLowerCase()+".";
			cat.save();
		}		
	}
	
	private void createProjects() {
		int nbFollowersMax = 15;
		int nbContribMax = 5;
		projects = new ArrayList<Project>(nbProj);
		for (int i = 0; i < nbProj; i++) {
			Project proj = new Project();
			projects.add(proj);
			
			proj.name = "Projet "+i;
			proj.description = "Description du projet "+i+".";
			proj.website = "www.projet"+i+".fr";
			proj.currentState = enumStates.values()[rand.nextInt(4)];
			if(proj.currentState == enumStates.RES)
			{
				User res = users.get(rand.nextInt(nbUsers));
				proj.resurrector = res;
				res.projectsResurrected.add(proj);
			}
			proj.creationDate = randomDate();
			
			Category cat = categories.get(rand.nextInt(nbCateg));
			proj.category = cat;
			cat.projects.add(proj);
			
			User creator = users.get(rand.nextInt(nbUsers));
			proj.creator = creator;
			creator.projectsCreated.add(proj);
			
			int nbFollowers = rand.nextInt(nbFollowersMax+1);
			proj.followers = randomUserSet(nbFollowers);
			for (User follower : proj.followers) {
				follower.projectsFollowed.add(proj);
			}
			
			int nbContrib = rand.nextInt(nbContribMax+1);
			proj.contributors = randomUserSet(nbContrib);
			for (User contributor : proj.contributors) {
				contributor.projectsContributed.add(proj);
			}
						
			proj.save();
		}
	}
	
	private void createPropResu() {
		int nbPropRes = 20;
		for (int i = 0; i < nbPropRes; i++) {
			RezQuery rezQuery = new RezQuery();
			
			Project proj = projects.get(rand.nextInt(nbProj));
			rezQuery.project = proj;
			proj.propResSet.add(rezQuery);
			
			User user = users.get(rand.nextInt(nbUsers));
			rezQuery.user = user;
			user.rezQuerySet.add(rezQuery);
			
			rezQuery.motivation = "Moi "+user.pseudo+" a de bonnes raisons de vouloir reprendre le projet "+proj.name+".";
			
			rezQuery.save();
		}
	}
	
	private void createRewards() {
		int nbRewards = 20;
		for (int i = 0; i < nbRewards; i++) {
			Reward reward = new Reward();
			
			Project proj = projects.get(rand.nextInt(nbProj));
			reward.project = proj;
			proj.rewards.add(reward);
			
			User user = users.get(rand.nextInt(nbUsers));
			reward.rewarder = user;
			user.rewards.add(reward);
			
			reward.amount = rand.nextInt(1000)+50;
			
			reward.save();
		}
	}
	
	@Override
	public void onStart(Application app) {
		super.onStart(app);
		if(User.find.all().size() == 0)
		{
			clearDatabase();
			createUsers();
			createNotifications();
			createCategories();
			createProjects();
			createPropResu();
			createRewards();
		}
	}

}
